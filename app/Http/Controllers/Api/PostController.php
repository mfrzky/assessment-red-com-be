<?php

namespace App\Http\Controllers\Api;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\PostResource;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller {

    public function index() {
        return new PostResource(Post::all());
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'desc' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post = Post::create([
            'title'     => $request->title,
            'desc'   => $request->desc
        ]);

        return new PostResource($post);
    }

    public function show(Post $post) {
        return new PostResource($post);
    }

    public function update(Request $request, Post $post) {
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'desc' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $post->update([
            'title'     => $request->title,
            'desc'   => $request->desc
        ]);

        return new PostResource($post);
    }

    public function destroy(Post $post) {
        $post->delete();
        
        return new PostResource($post);
    }
}